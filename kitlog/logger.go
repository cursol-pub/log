package kitlog

import (
	"context"
	kitlog "github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"gitlab.com/cursol-pub/log"
	"os"
)

var _ log.Logger = (*kitLogger)(nil)

const (
	timeKey    = "ts"
	callerKey  = "caller"
	messageKey = "msg"
)

type kitLogger struct {
	logger kitlog.Logger
}

func NewLogger() log.Logger {
	logger := kitlog.NewJSONLogger(kitlog.NewSyncWriter(os.Stderr))
	logger = kitlog.WithPrefix(logger,
		timeKey, kitlog.DefaultTimestampUTC,
		callerKey, kitlog.DefaultCaller)
	return &kitLogger{logger}
}

// NewContext returns a context has a zap logger with the extra fields added
func (z *kitLogger) NewContext(ctx context.Context, args ...any) context.Context {
	return context.WithValue(ctx, log.LoggerKey, z.WithContext(ctx).With(args))
}

// WithContext returns zap logger with as much context as possible
func (z *kitLogger) WithContext(ctx context.Context) log.Logger {
	if ctx == nil {
		return z
	}
	if ctxLogger, ok := ctx.Value(log.LoggerKey).(log.Logger); ok {
		return ctxLogger
	} else {
		return z
	}
}

func (z *kitLogger) Log(_ context.Context, message string, args ...any) {
	fs := make([]any, len(args)+2)
	z.buildFields(fs, message, args)
	_ = z.logger.Log(fs...)
}

func (z *kitLogger) Debug(_ context.Context, message string, args ...any) {
	fs := make([]any, len(args)+2)
	z.buildFields(fs, message, args)
	_ = level.Debug(z.logger).Log(fs...)
}

func (z *kitLogger) Info(_ context.Context, message string, args ...any) {
	fs := make([]any, len(args)+2)
	z.buildFields(fs, message, args)
	_ = level.Info(z.logger).Log(fs...)
}

func (z *kitLogger) Warn(_ context.Context, message string, args ...any) {
	fs := make([]any, len(args)+2)
	z.buildFields(fs, message, args)
	_ = level.Warn(z.logger).Log(fs...)
}

func (z *kitLogger) Error(_ context.Context, message string, args ...any) {
	fs := make([]any, len(args)+2)
	z.buildFields(fs, message, args)
	_ = level.Error(z.logger).Log(fs...)
}

func (z *kitLogger) Fatal(ctx context.Context, message string, args ...any) {
	z.Error(ctx, message, args)
}

func (z *kitLogger) With(args ...any) log.Logger {
	z.logger = kitlog.With(z.logger, args...)
	return z
}

func (*kitLogger) buildFields(fs []any, message string, args []any) {
	fs[0] = messageKey
	fs[1] = message
	ln := len(args)
	for i := 0; i < ln; i++ {
		fs[i+2] = args[i]
	}
}
