package kitlog

import (
	"context"
	"gitlab.com/cursol-pub/log"
	"sync"
	"testing"
)

func makeLogger() (logger log.Logger) {
	sync.OnceFunc(func() {
		logger = NewLogger()
	})()

	return
}

func Test_kitLogger_Log(t *testing.T) {
	type args struct {
		ctx     context.Context
		message string
		args    []any
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Test Log",
			args: args{
				ctx:     context.TODO(),
				message: "log message",
				args:    []any{"one", 1, "id", "IDOO13", "valid", true},
			},
		},
	}

	z := makeLogger()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			z.Log(tt.args.ctx, tt.args.message, tt.args.args...)
		})
	}
}

func Benchmark_kitLogger_Info(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(30)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Info(ctx, "Benchmark_kitLogger_Info",
					"step", i,
					"group", "Benchmark",
					"component", "kitLogger",
					"stable", true,
					"err", nil)
			}
		})
	}
}
