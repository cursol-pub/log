package slog

import (
	"context"
	"fmt"
	"gitlab.com/cursol-pub/log"
	"log/slog"
	"os"
)

var _ log.Logger = (*slogLogger)(nil)

const LevelFatal slog.Level = 13

type slogLogger struct {
	logger *slog.Logger
}

func NewLogger() log.Logger {
	h := slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{
		AddSource: false,
		Level:     slog.LevelInfo,
	})
	logger := slog.New(h)
	return &slogLogger{logger}
}

// NewContext returns a context has a zap logger with the extra fields added
func (z *slogLogger) NewContext(ctx context.Context, args ...any) context.Context {
	return context.WithValue(ctx, log.LoggerKey, z.WithContext(ctx).With(args))
}

// WithContext returns zap logger with as much context as possible
func (z *slogLogger) WithContext(ctx context.Context) log.Logger {
	if ctx == nil {
		return z
	}
	if ctxLogger, ok := ctx.Value(log.LoggerKey).(log.Logger); ok {
		return ctxLogger
	} else {
		return z
	}
}

func (z *slogLogger) Log(ctx context.Context, message string, args ...any) {
	fs := make([]slog.Attr, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.LogAttrs(ctx, slog.LevelInfo, message, fs...)
}

func (z *slogLogger) Debug(ctx context.Context, message string, args ...any) {
	fs := make([]slog.Attr, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.LogAttrs(ctx, slog.LevelDebug, message, fs...)
}

func (z *slogLogger) Info(ctx context.Context, message string, args ...any) {
	fs := make([]slog.Attr, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.LogAttrs(ctx, slog.LevelInfo, message, fs...)
}

func (z *slogLogger) Warn(ctx context.Context, message string, args ...any) {
	fs := make([]slog.Attr, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.LogAttrs(ctx, slog.LevelWarn, message, fs...)
}

func (z *slogLogger) Error(ctx context.Context, message string, args ...any) {
	fs := make([]slog.Attr, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.LogAttrs(ctx, slog.LevelError, message, fs...)
}

func (z *slogLogger) Fatal(ctx context.Context, message string, args ...any) {
	fs := make([]slog.Attr, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.LogAttrs(ctx, LevelFatal, message, fs...)
}

func (z *slogLogger) With(args ...any) log.Logger {
	fs := make([]slog.Attr, (len(args)+1)/2)
	z.buildFields(args, fs)

	fsany := make([]any, len(fs))
	for i, v := range fs {
		fsany[i] = v
	}

	z.logger.With(fsany...)
	return z
}

func (z *slogLogger) buildFields(args []any, fs []slog.Attr) {
	l := len(args)
	for i := 0; i < l; i += 2 {
		j := i + 1
		var k string
		var v any
		if j == l {
			k = log.KeyCause
			v = args[i]
		} else {
			k = args[i].(string)
			v = args[j]
		}
		z.appendField(i/2, k, v, fs)
	}
}

func (z *slogLogger) appendField(i int, k string, v any, fs []slog.Attr) {
	switch v.(type) {
	case string:
		fs[i] = slog.String(k, v.(string))
	case int64:
		fs[i] = slog.Int64(k, v.(int64))
	case fmt.Stringer:
		fs[i] = slog.String(k, v.(fmt.Stringer).String())
	default:
		fs[i] = slog.Any(k, v)
	}
}
