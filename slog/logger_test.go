package slog

import (
	"context"
	"errors"
	"gitlab.com/cursol-pub/log"
	"log/slog"
	"sync"
	"testing"
)

func makeLogger() (logger log.Logger) {
	sync.OnceFunc(func() {
		logger = NewLogger()
	})()

	return
}

func Test_slogLogger_Log(t *testing.T) {
	type args struct {
		ctx     context.Context
		message string
		args    []any
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Test Log",
			args: args{
				ctx:     context.TODO(),
				message: "log message",
				args:    []any{"one", 1, "id", "IDOO13", "valid", true},
			},
		},
	}

	z := makeLogger()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			z.Log(tt.args.ctx, tt.args.message, tt.args.args...)
		})
	}
}

func Benchmark_Logger_Debug(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(30)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Debug(ctx, "Benchmark_slogLogger_Debug",
					"step", i,
					"group", "Benchmark",
					"component", "slogLogger",
					"logLevel", slog.LevelDebug,
					"stable", true,
					"err", nil)
			}
		})
	}
}

func Benchmark_Logger_Error(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(10)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Error(ctx, "Benchmark_slogLogger_Error",
					"step", i,
					"group", "Benchmark",
					"component", "slogLogger",
					"logLevel", slog.LevelError,
					"stable", false,
					"err", errors.New("failed Benchmark_slogLogger_Error"))
			}
		})
	}
}

func Benchmark_Logger_Info(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(10)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Info(ctx, "Benchmark_slogLogger_Info",
					"step", i,
					"group", "Benchmark",
					"component", "slogLogger",
					"logLevel", slog.LevelInfo,
					"stable", true,
					"err", nil)
			}
		})
	}
}

func Benchmark_Logger_Warn(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(30)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Warn(ctx, "Benchmark_slogLogger_Warn",
					"step", i,
					"group", "Benchmark",
					"component", "slogLogger",
					"logLevel", slog.LevelWarn,
					"stable", true,
					"err", nil)
			}
		})
	}
}
