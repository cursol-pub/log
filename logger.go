package log

import (
	"context"
)

const (
	LoggerKey loggerKeyType = iota

	KeyCause = "cause"
)

type loggerKeyType int

type Logger interface {
	WithContext(ctx context.Context) Logger
	NewContext(ctx context.Context, args ...any) context.Context
	Log(ctx context.Context, message string, args ...any)
	Debug(ctx context.Context, message string, args ...any)
	Info(ctx context.Context, message string, args ...any)
	Warn(ctx context.Context, message string, args ...any)
	Error(ctx context.Context, message string, args ...any)
	Fatal(ctx context.Context, message string, args ...any)
	With(args ...any) Logger
}
