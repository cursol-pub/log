module gitlab.com/cursol-pub/log

go 1.21.1

require (
	github.com/aws/aws-xray-sdk-go v1.8.1
	github.com/go-kit/log v0.2.1
	go.uber.org/zap v1.26.0
)

require (
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
)
