package xraylog

import (
	"context"
	"fmt"
	"github.com/aws/aws-xray-sdk-go/xraylog"
	"gitlab.com/cursol-pub/log"
)

type xrayLogger struct {
	l log.Logger
}

func (x *xrayLogger) Log(level xraylog.LogLevel, msg fmt.Stringer) {
	ctx := context.TODO()
	message := msg.String()

	switch level {
	case xraylog.LogLevelWarn:
		x.l.Warn(ctx, message)
	case xraylog.LogLevelInfo:
		x.l.Info(ctx, message)
	case xraylog.LogLevelDebug:
		x.l.Debug(ctx, message)
	case xraylog.LogLevelError:
		x.l.Error(ctx, message)
	default:
	}
}

func NewWith(l log.Logger) xraylog.Logger {
	return &xrayLogger{l}
}
