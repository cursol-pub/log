package xraylog

import (
	"context"
	"github.com/aws/aws-xray-sdk-go/strategy/ctxmissing"
	"gitlab.com/cursol-pub/log"
)

const (
	logMsg   = "Suppressing AWS X-Ray context missing panic"
	keyCause = "cause"
)

type warnErrorStrategy struct {
	logger log.Logger
}

func (s *warnErrorStrategy) ContextMissing(err any) {
	s.logger.Warn(context.TODO(), logMsg, keyCause, err)
}

func NewWarnErrorStrategy(logger log.Logger) ctxmissing.Strategy {
	return &warnErrorStrategy{logger}
}
