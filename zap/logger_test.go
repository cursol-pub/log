package zap

import (
	"context"
	"errors"
	"gitlab.com/cursol-pub/log"
	"go.uber.org/zap"
	"sync"
	"testing"
)

func makeLogger() (logger log.Logger) {
	sync.OnceFunc(func() {
		logger, _ = NewLogger()
	})()

	return
}

func Benchmark_Logger_Debug(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(30)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Debug(ctx, "Benchmark_zapLogger_Debug",
					"step", i,
					"group", "Benchmark",
					"component", "zapLogger",
					"logLevel", zap.DebugLevel,
					"stable", true,
					"err", nil)
			}
		})
	}
}

func Benchmark_Logger_Error(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(30)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Error(ctx, "Benchmark_zapLogger_Error",
					"step", i,
					"group", "Benchmark",
					"component", "zapLogger",
					"logLevel", zap.ErrorLevel,
					"stable", false,
					"err", errors.New("failed Benchmark_zapLogger_Error"))
			}
		})
	}
}

func Benchmark_Logger_Info(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(10)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Info(ctx, "Benchmark_zapLogger_Info",
					"step", i,
					"group", "Benchmark",
					"component", "zapLogger",
					"logLevel", zap.InfoLevel,
					"stable", true,
					"err", nil)
			}
		})
	}
}

func Benchmark_Logger_Warn(b *testing.B) {
	ctx := context.TODO()
	logger := makeLogger()

	b.SetParallelism(30)

	for i := 0; i < b.N; i++ {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				logger.Warn(ctx, "Benchmark_zapLogger_Warn",
					"step", i,
					"group", "Benchmark",
					"component", "zapLogger",
					"logLevel", zap.WarnLevel,
					"stable", true,
					"err", nil)
			}
		})
	}
}
