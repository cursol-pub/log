package zap

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/cursol-pub/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var _ log.Logger = (*zapLogger)(nil)

const (
	timeKey = "time"
)

var errFailedCreateLogger = errors.New("failed to create zap logger")

type zapLogger struct {
	logger *zap.Logger
}

func NewLogger() (log.Logger, error) {
	cfg := zap.NewProductionConfig()
	cfg.EncoderConfig = getEncoderConfig()
	logger, err := cfg.Build()
	if err != nil {
		return nil, fmt.Errorf("%v: %w", err, errFailedCreateLogger)
	}
	return &zapLogger{logger}, nil
}

// NewContext returns a context has a zap logger with the extra fields added
func (z *zapLogger) NewContext(ctx context.Context, args ...any) context.Context {
	return context.WithValue(ctx, log.LoggerKey, z.WithContext(ctx).With(args))
}

// WithContext returns zap logger with as much context as possible
func (z *zapLogger) WithContext(ctx context.Context) log.Logger {
	if ctx == nil {
		return z
	}
	if ctxLogger, ok := ctx.Value(log.LoggerKey).(log.Logger); ok {
		return ctxLogger
	} else {
		return z
	}
}

func getEncoderConfig() zapcore.EncoderConfig {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	encoderConfig.TimeKey = timeKey
	return encoderConfig
}

func (z *zapLogger) Log(_ context.Context, message string, args ...any) {
	fs := make([]zap.Field, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.Log(zapcore.InfoLevel, message, fs...)
}

func (z *zapLogger) Debug(_ context.Context, message string, args ...any) {
	fs := make([]zap.Field, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.Debug(message, fs...)
}

func (z *zapLogger) Info(_ context.Context, message string, args ...any) {
	fs := make([]zap.Field, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.Info(message, fs...)
}

func (z *zapLogger) Warn(_ context.Context, message string, args ...any) {
	fs := make([]zap.Field, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.Warn(message, fs...)
}

func (z *zapLogger) Error(_ context.Context, message string, args ...any) {
	fs := make([]zap.Field, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.Error(message, fs...)
}

func (z *zapLogger) Fatal(_ context.Context, message string, args ...any) {
	fs := make([]zap.Field, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.Fatal(message, fs...)
}

func (z *zapLogger) With(args ...any) log.Logger {
	fs := make([]zap.Field, (len(args)+1)/2)
	z.buildFields(args, fs)
	z.logger.With(fs...)
	return z
}

func (z *zapLogger) buildFields(args []any, fs []zap.Field) {
	l := len(args)
	for i := 0; i < l; i += 2 {
		j := i + 1
		var k string
		var v any
		if j == l {
			k = log.KeyCause
			v = args[i]
		} else {
			k = args[i].(string)
			v = args[j]
		}
		z.appendField(i/2, k, v, fs)
	}
}

func (z *zapLogger) appendField(i int, k string, v any, fs []zap.Field) {
	switch v.(type) {
	case string:
		fs[i] = zap.String(k, v.(string))
	case int64:
		fs[i] = zap.Int64(k, v.(int64))
	case error:
		fs[i] = zap.NamedError(k, v.(error))
	case fmt.Stringer:
		fs[i] = zap.String(k, v.(fmt.Stringer).String())
	default:
		fs[i] = zap.Any(k, v)
	}
}
